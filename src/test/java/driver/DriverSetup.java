package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;

public class DriverSetup {

    private static String browser = "chrome";
    private static String os = "linux";

    //    private static String node = "192.168.2.168:4444";
    private static String node = "172.23.0.2:4444";

    private static String type = "remote";

    private final static String driversPath = Paths.get(System.getProperty("user.dir"), "drivers").toString();

    private static WebDriver driver;

    public static WebDriver getDriver() {
        return driver;
    }

    public static void startDriver() {

        if (type.equalsIgnoreCase("local")) {
            if (browser.equalsIgnoreCase("firefox")) {

                System.setProperty("webdriver.gecko.driver", Paths.get(driversPath, "geckodriver.exe").toString());
                driver = new FirefoxDriver();

            } else {

                System.setProperty("webdriver.chrome.driver", Paths.get(driversPath, "chromedriver.exe").toString());
                driver = new ChromeDriver();
            }

            return;
        }

        try {
            if (browser.equalsIgnoreCase("chrome")) {

                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setCapability("platform", os);
                chromeOptions.setCapability("timeout", 10);
                chromeOptions.setCapability("newSessionWaitTimeout ", 25000);
                // chromeOptions.addArguments("--headless");
                driver = new RemoteWebDriver(new URL("http://" + node + "/wd/hub"), chromeOptions);

            } else if (browser.equalsIgnoreCase("firefox")) {

                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.setCapability("platform", os);
                driver = new RemoteWebDriver(new URL("http://" + node + "/wd/hub"), firefoxOptions);
            }

        } catch (MalformedURLException meu) {
            meu.printStackTrace();
        }
    }



    public static void tearDown() {
        if (driver != null) {
            driver.close();
        }
    }



}
