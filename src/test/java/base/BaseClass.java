package base;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import driver.DriverSetup;

public class BaseClass {

    private String baseUrl = "http://172.23.0.5:8080";
//    private String baseUrl = "http://192.168.2.168:8080";
//    private String baseUrl = "http://google.com";

    private WebDriver driver() {
        return DriverSetup.getDriver();
    }

    @BeforeClass
    public void setupDriver() {
        DriverSetup.startDriver();
        driver().get(baseUrl);
    }

    @BeforeMethod
    public void beginTest() {
        new Action().logout();
        driver().get(baseUrl);
    }

    @AfterMethod
    public void endTest() {
        BasePageObject.waitForSeconds(5);
        new Action().logout();
    }

    @AfterClass
    public void stopDriver() {
        DriverSetup.tearDown();
    }

}
