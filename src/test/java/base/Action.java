package base;

import io.qameta.allure.Step;
import pages.LoginPage;
import pages.Toolbar;

public class Action {

    @Step
    public void login(String userName, String password) {
        LoginPage loginPage = new LoginPage();

        if (loginPage.isPageOpened()) {
            loginPage.typeUsername(userName);
            loginPage.typePassword(password);
            loginPage.submit();
        }
    }

    @Step
    public void loginRegular() {
        login("admin", "123456");
    }

    @Step
    public void logout() {
        Toolbar toolbar = new Toolbar();

        if (toolbar.isVisible()) {
            toolbar.logout();
        }
    }
}
