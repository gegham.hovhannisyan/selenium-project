package base;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import driver.DriverSetup;

public class BasePageObject {

    private WebDriver driver = DriverSetup.getDriver();

    protected WebDriver driver() {
        return driver;
    }

    protected void waitForVisibility(WebElement element, Integer... seconds) {
        int timeout = seconds.length == 0 ? 1 : seconds[0];
        waitFor(timeout).until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitForInvisibility(WebElement element, Integer... seconds) {
        int timeout = seconds.length == 0 ? 1 : seconds[0];
        waitFor(timeout).until(ExpectedConditions.invisibilityOf(element));
    }

    protected void waitForVisibility(By locator, Integer... seconds) {
        int timeout = seconds.length == 0 ? 1 : seconds[0];
        waitFor(timeout).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    protected boolean isPageOpened(WebElement element) {
        try {
            waitForVisibility(element);
            return true;
        } catch (TimeoutException te) {
            return false;
        }
    }

    protected boolean isVisible(By locator) {
        try {
            waitForVisibility(locator);
            return true;
        } catch (TimeoutException te) {
            return false;
        }
    }

    protected boolean isVisible(WebElement element) {
        try {
            waitForVisibility(element);
            return true;
        } catch (TimeoutException te) {
            return false;
        }
    }

    protected void type(String text, WebElement element) {
        element.sendKeys(text);
    }

    private WebDriverWait waitFor(int seconds) {
        return new WebDriverWait(driver(), seconds);
    }

    protected void click(WebElement element) {
        element.click();
    }

    protected void scrollToElement(WebElement element) {
        Actions actions = new Actions(driver());
        actions.moveToElement(element);
        actions.perform();
    }

    protected static void waitForSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException ignore) {}
    }

}
