package tests;

import org.testng.annotations.Test;
import base.Action;
import base.BaseClass;
import pages.LoginPage;
import pages.Toolbar;

public class LoginLogoutTests extends BaseClass {

    @Test(testName = "LLT-1", description = "Testing the login functionality")
    public void loginTest() {

        new Action().loginRegular();

        new Toolbar().verify();
    }

    @Test(testName = "LLT-2", description = "Testing the logout functionality")
    public void logoutTest() {

        new Action().loginRegular();

        new Action().logout();

        new LoginPage().waitForPageToLoad();
    }
}
