package tests;

import org.testng.annotations.Test;
import base.Action;
import base.BaseClass;
import pages.JobCreatePage;
import pages.JobPage;
import pages.Navigator;

import java.util.Random;

public class CreationNewJobTests extends BaseClass {

    @Test(testName = "CJ-1", description = "Create free style job")
    public void createFreeStyleJobTest() {

        new Action().loginRegular();
        new Navigator().clickNewItem();

        int intName = new Random().nextInt(10000);
        JobCreatePage jobCreatePage = new JobCreatePage();
        jobCreatePage.typeJobName(String.valueOf(intName));
        jobCreatePage.clickFreeStyle();

        jobCreatePage.clickOk();

        new JobPage().waitForLoaderInvisible();
    }
}
