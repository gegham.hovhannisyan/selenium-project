package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import base.BasePageObject;

public class LoginPage extends BasePageObject {

    @FindBy(id = "j_username")
    WebElement usernameElement;

    @FindBy(name = "j_password")
    WebElement passElement;

    @FindBy(name = "Submit")
    WebElement submitElement;

    @FindBy(className = "logo")
    WebElement logo;

    public LoginPage() {
        PageFactory.initElements(driver(), this);
    }

    @Step
    public LoginPage waitForPageToLoad() {
        waitForVisibility(logo);
        return this;
    }

    public boolean isPageOpened() {
        return isPageOpened(logo);
    }

    @Step
    public void typeUsername(String userName) {
        type(userName, usernameElement);
    }

    @Step
    public void typePassword(String password) {
        type(password, passElement);
    }

    @Step
    public void submit() {
        click(submitElement);
    }
}
