package pages;

import base.BasePageObject;
import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class JobCreatePage extends BasePageObject {

    @FindBy(id = "ok-button")
    WebElement ok;

    @FindBy(name = "name")
    WebElement name;

    @FindBy(xpath = "//span[contains(text(),'Freestyle project')]")
    WebElement free;

    public JobCreatePage() {
        PageFactory.initElements(driver(), this);
    }

    @Step
    public void typeJobName(String newName) {
        waitForVisibility(name);
        type(newName, name);
    }

    @Step
    public void clickFreeStyle() {
        scrollToElement(free);
        click(free);
    }

    @Step
    public void clickOk() {
        click(ok);
    }


}