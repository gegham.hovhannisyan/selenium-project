package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import base.BasePageObject;

public class JobPage extends BasePageObject {

    @FindBy(className = "behavior-loading")
    WebElement loader;

    public JobPage() {
        PageFactory.initElements(driver(), this);
    }

    public void waitForLoaderInvisible() {
        waitForInvisibility(loader, 60);
    }

}
