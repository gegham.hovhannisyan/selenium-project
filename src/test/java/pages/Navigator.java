package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import base.BasePageObject;

public class Navigator extends BasePageObject {

    @FindBy(xpath = " //a[contains(text(),'New Item')]")
    WebElement newItem;

    public Navigator() {
        PageFactory.initElements(driver(), this);
    }

    @Step
    public void clickNewItem() {
        click(newItem);
    }

}
