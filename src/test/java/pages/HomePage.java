package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import base.BasePageObject;

public class HomePage extends BasePageObject {

    @FindBy(id = "jenkins-home-icon")
    WebElement homeElement;

    public HomePage() {
        PageFactory.initElements(driver(), this);
    }

    @Step
    public void waitForPageToLoad() {
        waitForVisibility(homeElement);
    }

    @Step
    public boolean isPageOpened() {
        return super.isPageOpened(homeElement);
    }

}
