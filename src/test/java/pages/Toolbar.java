package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import base.BasePageObject;

public class Toolbar extends BasePageObject {

    @FindBy(id = "jenkins-head-icon")
    WebElement headIcon;

    @FindBy(id = "jenkins-name-icon")
    WebElement nameIcon;

    @FindBy(id = "searchform")
    WebElement searchForm;

    @FindBy(linkText = "Jenkins Manager")
    WebElement managerButton;

    @FindBy(className = "icon-help")
    WebElement helpIcon;

    @FindBy(linkText = "log out")
    WebElement logout;

    public Toolbar () {
        PageFactory.initElements(driver(), this);
    }

    public boolean isVisible() {
        return super.isVisible(logout);
    }

    public void logout() {
        click(logout);
    }

    public void verify() {
        Assert.assertTrue(isVisible(headIcon));
        Assert.assertTrue(isVisible(nameIcon));
        Assert.assertTrue(isVisible(searchForm));
        Assert.assertTrue(isVisible(managerButton));
        Assert.assertTrue(isVisible(helpIcon));
        Assert.assertTrue(isVisible(logout));
    }

}
